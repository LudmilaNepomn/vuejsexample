
Vue.component('simple-counter', {
    template: '<button v-on:click="clientcounter += 1">{{ clientcounter }}</button>',
    data: function () {
        return {
            clientcounter: 0
        }
    }
})

Vue.component('server-counter', {
    template: '<button v-on:click="getServerCounter()">{{ servercounter }}</button>',
    data: function () {
        return {
            servercounter: '0',
            socket: socket
        }
    },
    ready: function () {
        socket.onopen = function() {
            console.log("Соединение установлено.");
        };
    },
    methods: {
        getServerCounter: function() {
            var that = this
            socket.send("message to server")
            socket.onmessage = function(event){
                console.log("Hello")
                that.servercounter = event.data.toString()
                console.log(event.data.toString())
            };
        }

    }
})

new Vue({
    el: '#example'
})


const NotFound = { template: '<p>Страница не найдена</p>' }
const Home = { template: '<p>Страница Home</p>' }
const routes = {
    '/': Home
}
new Vue({
    el: '#app',
    data: {
        currentRoute: window.location.pathname
    },
    computed: {
        ViewComponent () {
            return routes[this.currentRoute] || NotFound
        }
    },
    render (h) { return h(this.ViewComponent) }
})