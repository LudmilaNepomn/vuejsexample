import com.google.inject.AbstractModule;

import services.AtomicCounter;
import services.Counter;

public class Module extends AbstractModule {

    @Override
    public void configure() {
        // Set AtomicCounter as the implementation for Counter.
        bind(Counter.class).to(AtomicCounter.class);


    }

}
