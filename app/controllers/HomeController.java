package controllers;

import akka.stream.javadsl.Flow;
import play.mvc.*;

import services.Counter;
import views.html.*;

import javax.inject.Inject;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    private final Counter counter;

    @Inject
    public HomeController(Counter counter) {
        this.counter = counter;
    }

    public Result index() {
        return ok(index.render("Your new application is ready"));
    }

//    public Result count() {
//        return ok(Integer.toString(counter.nextCount()));
//    }

    public WebSocket socket() {
        return WebSocket.Text.accept(request -> {
            return Flow.<String>create().map(msg -> {
                System.out.println(msg);
                String str = Integer.toString(counter.nextCount());
                System.out.println("message to client: "+str);
                return str;
            });
        });
    }



}
